# SECURICAM USER MANUAL:#

**Note:** SecuriCam requires that the computer be connected to the internet and have a working webcam connected to the computer.
## FIRST TIME SETUP: Windows ##
1. Download OpenCV from:

	http://sourceforge.net/projects/opencvlibrary/files/opencv-win/2.4.9/opencv-2.4.9.exe/download

2. Run the executable for OpenCV downloaded in step 1
Jars to install and include in project:
3. Download JavaCV 0.7 from:
	 https://code.google.com/p/javacv/downloads/detail?name=javacv-0.7-bin.zip
4. Extract the zip file to a location of your choice, remembering this location for future use.
5. In eclipse navigate to Project --> Properties --> Java Build Path -->Libraries   and click "Add external jars." Locate the Java CV files extracted in step 4 and click OK.
6. Set up your path variable so that Eclipse can find the OpenCV libraries (which, from above, are in C:\opencv). 

	a) Right Click on "Computer" in the Windows Explorer and select "Properties"

	b) Click "Advanced system settings", go to the "Advanced" tab, and select  "Environment 	Variables..."

	c) In the "System variables" panel, go down to "Path" and select "Edit..."

	d) Add to the end of the list (don't overwrite what's already there!), separated 	by semicolons, 	two folders — for OpenCV and Tbb (you can browse in the Windows Explorer to double check 	that these folders exist):

i) 32 bit:
```
#!

;C:\opencv\build\x86\vc10\bin;C:\opencv\build\common\tbb\ia32\vc10
```
ii) 64 bit:
```
#!

;C:\opencv\build\x64\vc10\bin;C:\opencv\build\common\tbb\intel64\vc10
```

a) ![a.png](https://bitbucket.org/repo/M5kxkj/images/1972943687-a.png)
 
b) ![b.png](https://bitbucket.org/repo/M5kxkj/images/456324121-b.png)
 
c) ![c.png](https://bitbucket.org/repo/M5kxkj/images/4058708469-c.png)

7) Download JavaMail jar file from here:
	https://java.net/projects/javamail/pages/Home
and include it in the project in the same way the javacv jars were included.

8) Download JMF (Cross-platform Java - jmf-2_1_1e-alljava.zip) here:
	http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-client-419417.html#7372-jmf-2.1.1e-oth-JPR
and extract the folder to a location of your choosing, again remembering this location.
9) Also download the Performance pack for your operating system and run the executable file from the link in step 8.

10) Restart your computer as prompted

11) Open the 'config.properties' file found in the root folder of the SecuriCam application in a text editor of your choice and fill in the details of your email and password for a gmail account, for the ToEmail and FromEmail, (these may be the same address). Also, change the pin to a 4 digit pin that you can enter to turn off SecuriCam when it is running.

12) First time setup is now complete, compile and run the program.