import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
/** 
 * @author Josh Carolan 
 * SID:311181058
 */

public class UserInterface {

	public JFrame frame = null;
	public JLabel pinLabel, startLabel1,startLabel2,startLabel3,startLabel4;
	public JButton start, close;
	public JPasswordField pinField;

	public Filter f = new Filter();

	public void showGUI() throws IOException {

		pinField = new javax.swing.JPasswordField(4);
		pinLabel = new javax.swing.JLabel();
		pinLabel.setText("Enter password to turn SecuriCam off");

		frame = new JFrame("SecuriCam");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // <-
																	// enforces
																	// the
																	// closing
																	// on pin
																	// only
		startLabel1 = new javax.swing.JLabel();
		startLabel2 = new javax.swing.JLabel();
		startLabel3 = new javax.swing.JLabel();
		startLabel4 = new javax.swing.JLabel();
		
		startLabel1.setText("Once Start Surveillance is clicked you will");
		startLabel2.setText("have 10 seconds to position the camera followed");
		startLabel3.setText("by 10 seconds to leave the room before ");
		startLabel4.setText("the application starts.       ");
		frame.setResizable(true);
		frame.setVisible(true);
		frame.setLayout(new FlowLayout());

		start = new JButton("Start Surveilance");
		start.addActionListener(new MyButtonHandler());
		frame.add(startLabel1);
		frame.add(startLabel2);
		frame.add(startLabel3);
		frame.add(startLabel4);
		frame.add(start);

		close = new JButton("Stop Surveilance");
		close.addActionListener(new MyButtonHandler());
		frame.add(pinLabel);
		frame.add(pinField);
		frame.add(close);

		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				// do nothing
				JOptionPane
						.showMessageDialog(
								null,
								"You must enter the pin and click Stop Surveilance to close this program",
								"Error", JOptionPane.INFORMATION_MESSAGE);

			}
		});

		frame.validate();
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
	}

	// handles all of the actions for the buttons on the GUI
	private class MyButtonHandler implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// start the surveillance
			if (e.getActionCommand().equals(start.getText())) {
				// need a new thread to run the surveillance separate to the GUI
				// to ensure the GUI remains responsive to the stop command.
				Thread t = new Thread() {
					@Override
					public void run() {
						try {
							Camera.show();
							// sleep the program for 10 seconds to give the user
							// time to leave the room
							sleep(10000);
							// start the surveilance
							Camera.startSurveillance();
						} catch (InterruptedException ex) {
						}
					}
				};
				t.start();

			}
			// determine whether the user has entered the correct pin before
			// closing the application
			else if (e.getActionCommand().equals(close.getText())) {
				System.out.println(pinField.getPassword());
				String pin = String.valueOf(pinField.getPassword());
				ConfigReader cr = new ConfigReader();
				if (pin.equals(cr
						.getConfigurationFor("PasswordToCloseSecuriCam"))) {

					System.exit(0);
				} else {
					// incorrect pin entered
					JOptionPane
							.showMessageDialog(
									null,
									"Incorrect pin entered, please enter the correct pin and click 'Stop Surveilance' to exit",
									"Error", JOptionPane.INFORMATION_MESSAGE);
				}
			}

		}

	}

	// main method of the whole application, just shows the gui and everything
	// is started from the GUI
	public static void main(String[] args) throws IOException {
		UserInterface ui = new UserInterface();
		ui.showGUI();
	}
}
