import java.awt.image.BufferedImage;
import java.awt.Color;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_highgui.*;

/**
 * Filter is a class I made for another assignment and I have used some of the
 * methods in here to pre process the images for movement detection
 * 
 * @author Josh Carolan
 * SID: 311181058
 * 
 */
public class Filter {
	public BufferedImage greyscale(BufferedImage bufimg) {
		int height = bufimg.getHeight();
		int width = bufimg.getWidth();
		float gray;

		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				// Gray = 0.212671 * Red + 0.715160 * Green + 0.072169 * Blue
				Color c = new Color(bufimg.getRGB(col, row));
				gray = 0.212671f * (float) c.getRed() + 0.715160f
						* (float) c.getGreen() + 0.072169f
						* (float) c.getBlue();
				bufimg.setRGB(col, row, (int) gray << 16 | (int) gray << 8
						| (int) gray);
			}
		}

		// cvSaveImage("greyscale.jpg", greyscaleImage);
		return bufimg;
	}

	public IplImage colourAdjust(IplImage img, float redAdjust,
			float greenAdjust, float blueAdjust) {
		BufferedImage bufImg = img.getBufferedImage();
		int height = img.height();
		int width = img.width();

		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {

				Color c = new Color(bufImg.getRGB(col, row));

				bufImg.setRGB(
						col,
						row,
						(int) (redAdjust * (float) c.getRed()) << 16
								| (int) (greenAdjust * (float) c.getGreen()) << 8
								| (int) (blueAdjust * (float) c.getBlue()));
			}
		}
		IplImage colourAdjusted = IplImage.createFrom(bufImg);
		cvSaveImage("colourAdjusted.jpg", colourAdjusted);
		return colourAdjusted;
	}

	// if a pixel is less than the threshold set it to black else set it to
	// white
	public IplImage threshold(IplImage img, int threshold) {
		BufferedImage bufimg = img.getBufferedImage();
		int height = bufimg.getHeight();
		int width = bufimg.getWidth();
		for (int col = 1; col < width - 1; col++) {
			for (int row = 1; row < height - 1; row++) {
				Color c = new Color(bufimg.getRGB(col, row));
				if (c.getBlue() < threshold) {
					// set black
					bufimg.setRGB(col, row, 0 << 16 | 0 << 8 | 0);
				} else {
					// set white
					bufimg.setRGB(col, row, 255 << 16 | 255 << 8 | 255);
				}
			}
		}

		return IplImage.createFrom(bufimg);
	}

	public IplImage edgeDetect(IplImage img) {
		BufferedImage bufImg = img.getBufferedImage();
		BufferedImage edgeImg = bufImg;
		float[][] matrix = new float[3][3];
		int height = img.height();
		int width = img.width();

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				matrix[i][j] = -1f;
			}
		}
		matrix[1][1] = 9f;

		for (int col = 1; col < width - 1; col++) {
			for (int row = 1; row < height - 1; row++) {
				float sum = 0;
				for (int i = -1; i < 2; i++) {
					for (int j = -1; j < 2; j++) {
						Color c = new Color(bufImg.getRGB(col + i, row + j));
						float val = c.getRed();
						sum += matrix[i + 1][j + 1] * val;
					}
				}

				if (sum < 0) {
					sum = 0;
				} else if (sum > 255) {
					sum = 255;
				}
				Color newC = new Color((int) sum, (int) sum, (int) sum);
				edgeImg.setRGB(col, row, newC.getRGB());
			}
		}
		IplImage edgeDetected = IplImage.createFrom(edgeImg);
		cvSaveImage("edge_detect.jpg", edgeDetected);
		return edgeDetected;
	}

	public BufferedImage convolve(BufferedImage bufimg, float[][] kernel) {

		BufferedImage convImg = bufimg;

		int height = bufimg.getHeight();
		int width = bufimg.getWidth();

		for (int col = 1; col < width - 1; col++) {
			for (int row = 1; row < height - 1; row++) {
				float newRed = 0;
				float newGreen = 0;
				float newBlue = 0;
				for (int i = -1; i < 2; i++) {
					for (int j = -1; j < 2; j++) {
						Color c = new Color(bufimg.getRGB(col + i, row + j));
						newRed += kernel[i + 1][j + 1] * c.getRed();
						newGreen += kernel[i + 1][j + 1] * c.getGreen();
						newBlue += kernel[i + 1][j + 1] * c.getBlue();
					}
				}

				newRed = constrain(newRed, 0, 255);
				newGreen = constrain(newGreen, 0, 255);
				newBlue = constrain(newBlue, 0, 255);
				Color newC = new Color((int) newRed, (int) newGreen,
						(int) newBlue);
				convImg.setRGB(col, row, newC.getRGB());
			}
		}
		return convImg;
	}

	private float constrain(float x, int min, int max) {
		if (x < min) {
			x = (float) min;
		} else if (x > max) {
			x = (float) max;
		}
		return x;
	}

	public BufferedImage smooth(BufferedImage bufimg) {
		float[][] kernel = new float[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				kernel[i][j] = 0.111111f;
			}
		}

		BufferedImage smoothed = convolve(bufimg, kernel);
		// cvSaveImage("smoothed.jpg", smoothed);
		return smoothed;
	}

	public BufferedImage histogramEqualize(BufferedImage bufimg) {
		// equalises a greyscale image
		int[] histg = new int[256];
		for (int i = 0; i < 255; i++) {
			histg[i] = 0;
		}

		int height = bufimg.getHeight();
		int width = bufimg.getWidth();
		// get distrbution of grey values
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				Color c = new Color(bufimg.getRGB(col, row));
				int greyValue = c.getRed();
				histg[greyValue]++;
			}
		}
		// convert distribution to cdf
		int[] cdf = new int[256];
		int cdfmin = histg[0];
		cdf[0] = histg[0];
		for (int i = 1; i < histg.length; i++) {
			cdf[i] = cdf[i - 1] + histg[i];
			if (cdfmin > cdf[i]) {
				cdfmin = cdf[i];
			}
		}
		// calculate the new value for each gray scale
		int[] h = new int[256];
		for (int i = 0; i < histg.length; i++) {
			h[i] = (int) ((float) (cdf[i] - cdfmin) * 255 / (float) (width
					* height - cdfmin));
		}
		// modify the image to the equalised image
		// get the histogram
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				Color c = new Color(bufimg.getRGB(col, row));
				Color c2 = new Color(h[c.getRed()], h[c.getRed()],
						h[c.getRed()]);
				bufimg.setRGB(col, row, c2.getRGB());
			}
		}

		// cvSaveImage("equalized.jpg", equalized);
		return bufimg;

	}

	public BufferedImage sharpen(BufferedImage bufimg) {
		float[][] kernel = new float[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				kernel[i][j] = -1f;
			}
		}
		kernel[1][1] = 9f;
		BufferedImage sharpened = convolve(bufimg, kernel);

		return sharpened;
	}
}
