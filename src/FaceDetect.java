import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
/** 
 * @author Josh Carolan 
 * SID:311181058
 */
public class FaceDetect {
	public static final String XML_FILE = "./haarcascade_frontalface_default.xml";

	/**
	 * Uses the haarcascade classifier to determine if there is a face in the
	 * frame passed in as a parameter. only works for full frontal faces.
	 * 
	 * @param src
	 *            the image to detect the face in
	 * @return true if there is atlest one face in the image, otherwise false.
	 */
	boolean detectFaces(IplImage src) {
		CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(
				cvLoad(XML_FILE));
		CvMemStorage storage = CvMemStorage.create();
		CvSeq sign = cvHaarDetectObjects(src, cascade, storage, 1.5, 3,
				CV_HAAR_DO_CANNY_PRUNING);

		cvClearMemStorage(storage);

		int total_Faces = sign.total();

		if (total_Faces == 0) {
			return false;
		}
		return true;
	}
}
