import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
/** 
 * @author Josh Carolan 
 * SID:311181058
 */
public class SendEmail {
	/**
	 * Sends an email, attaching the 10 second video for the motion detection
	 * and a frame of the video where there is a face if one exists. This is
	 * done in a new thread since it takes approximately 3 minutes to send the
	 * email
	 * 
	 * @param imageFile
	 *            the name of the image that has a face detected in it to be
	 *            attached in the email. if it is empty there is no such frame
	 */
	String imageFile;
	String videoFile;

	public SendEmail(String iFile, String vFile) {
		imageFile = iFile;
		videoFile = vFile;
	}

	Thread t = new Thread() {
		public void run() {
			send();
			return;
		}

		public void send() {
			ConfigReader cr = new ConfigReader();

			SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss.SSS");
			Date now = new Date();
			String strDate = sdf.format(now);
			System.out.println("Starting send email function at:" + strDate);

			final String username = cr.getConfigurationFor("FromEmail");
			final String password = cr.getConfigurationFor("FromEmailPassword");

			// using gmails smtp server
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,
									password);
						}
					});

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(username));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(cr.getConfigurationFor("ToEmail")));
				message.setSubject("Motion Detected!");

				BodyPart messageBodyPart = new MimeBodyPart();
				if (imageFile.isEmpty()) {

					messageBodyPart
							.setText("The program couldn't get a clear shot of the intruders face");
				} else {
					messageBodyPart
							.setText("The program captured a  clear shot of the intruders face, and a picture is attached below");

				}

				// Create a multipart message
				Multipart multipart = new MimeMultipart();

				// Set text message part
				multipart.addBodyPart(messageBodyPart);

				// Part two is attachment

				// zip files up
				// ZipFiles zf = new ZipFiles();
				// String fileToAttach = zf.zip(videoFile, imageFile);
				if (!imageFile.isEmpty()) {
					messageBodyPart = new MimeBodyPart();
					DataSource imageSource = new FileDataSource(imageFile);
					messageBodyPart
							.setDataHandler(new DataHandler(imageSource));
					messageBodyPart.setFileName(imageFile);
					multipart.addBodyPart(messageBodyPart);
					System.out.println("finished attaching imageFile: "
							+ imageFile + " for videoFile: " + videoFile);
				}
				// attach Video file to the email
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(videoFile);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(videoFile);
				multipart.addBodyPart(messageBodyPart);

				message.setContent(multipart);

				Date now2 = new Date();
				String strDate2 = sdf.format(now2);
				System.out.println("Starting to send the email at:" + strDate2);

				Transport.send(message);

				Date now3 = new Date();
				String strDate3 = sdf.format(now3);
				System.out.println("Finished send email function at:"
						+ strDate3);
				

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		}
	};

}
