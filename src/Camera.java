import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
/** 
 * @author Josh Carolan 
 * SID:311181058
 */
public class Camera {

	static int movieNum;

	/**
	 * startSurveillance is the method that runs until the program is stopped by
	 * the user. it calls all of the other methods for movement detection and
	 * upon detecting movement, calls for the recordVideo method which records a
	 * 10 second video, checks for a face in any of the frames of the video and
	 * sends an email to the user with the video and the frame with a face in it
	 * 
	 */

	public static void startSurveillance() {
		OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0); // 1 for next
																// camera
		movieNum = 1;
		try {

			// start getting images from the webcam
			grabber.start();
			/*
			 * this loop is so that the camera is given time to adjust to
			 * lighting to get a good first frame for the motion detection.
			 */
			warmup(grabber);
			IplImage firstFrame = grabber.grab();
			// send the first frame to the motionDetect Constructor
			MotionDetect detector = new MotionDetect(firstFrame);
			IplImage currFrame;

			while (true) {
				currFrame = grabber.grab();
				if (currFrame != null) {
					if (detector.Detect(currFrame)) {
						/*
						 * hand over control of webcam to the record video
						 * method to capture the frames for the 30 second movie
						 */
						grabber.stop();
						recordVideo();
						// take control back of the webcam
						grabber.start();
						warmup(grabber);
						IplImage newbackground = grabber.grab();
						detector.setBackground(newbackground);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * records a short 10 second video, then instantiates a MakeCheckSend facade
	 * object to check the frames of this video for a shot of the intruders face
	 * and finally send the video and, if it exists, a shot of the intruders
	 * face in a new thread.
	 * 
	 * @throws com.googlecode.javacv.FrameGrabber.Exception
	 * @throws com.googlecode.javacv.FrameRecorder.Exception
	 */

	public static void recordVideo()
			throws com.googlecode.javacv.FrameGrabber.Exception,
			com.googlecode.javacv.FrameRecorder.Exception {
		int frameRate = 10;

		// get the camera, 0 is default camera
		OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
		grabber.setFrameRate(frameRate);
		grabber.start();
		int numOfFrames = 0;
		int maxFramesIn10Seconds = 100;
		IplImage currFrame = grabber.grab();
		int height = currFrame.height();
		int width = currFrame.width();
		try {
			while (numOfFrames < maxFramesIn10Seconds) {
				if (currFrame != null) {
					String i = "" + numOfFrames;
					if (numOfFrames < 10) {
						i = "00" + numOfFrames;
					} else if (numOfFrames < 100) {
						i = "0" + numOfFrames;
					}
					String FileName = i + ".jpg";
					cvSaveImage(FileName, currFrame);
					numOfFrames++;

				}
				currFrame = grabber.grab();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		grabber.stop();
		/*
		 * construct the MakeCheckSend object which is a facade for making the
		 * video from the jpg images, checking for a frame with a face in it and
		 * then, in a new thread, sending the video and the frame via email to
		 * the user .
		 */
		MakeCheckSend mcs = new MakeCheckSend();
		mcs.makeCheckAndSend(width, height, frameRate, maxFramesIn10Seconds,
				movieNum);
		// increment movieNum so that the program makes new files for videos
		// and face detected images
		movieNum++;

	}

	/**
	 * Just displays the camera view on the screen for 10 seconds so the user
	 * can set up the camera position to capture the area they want
	 */
	public static void show() {

		OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0); // 1 for next
		CanvasFrame canvas = new CanvasFrame("You have 10 seconds to position the camera then 10 seconds to leave the room");
		try {
			IplImage currFrame;
			grabber.start();
			long now = System.currentTimeMillis();
			// for 10 seconds
			while (System.currentTimeMillis() - now < 10000) {
				currFrame = grabber.grab();
				if (currFrame != null) {
					// 90_degrees_steps_anti_clockwise
					cvFlip(currFrame, currFrame, 1);// l-r =
					// show image on window
					canvas.showImage(currFrame);
				}
			}
			grabber.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
		canvas.dispose();

	}

	/**
	 * the web cam takes about 1 second to fully adjust to get a clear image so
	 * this warmup method runs the camera for 10 frames (1 second) so that it
	 * can focus
	 * 
	 * @param g
	 *            the grabber for the webcam that is to be warmed up
	 */
	public static void warmup(OpenCVFrameGrabber g) {
		for (int i = 1; i < 10; i++) {

			try {
				g.grab();
			} catch (com.googlecode.javacv.FrameGrabber.Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
