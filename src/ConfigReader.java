import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
/** 
 * @author Josh Carolan 
 * SID:311181058
 */
public class ConfigReader {

	Properties configFile = new Properties();
	InputStream input = null;
	
	public ConfigReader(){
		
		try {
			String filename= "config.properties";
			input = new FileInputStream(filename);
			if(input ==null ){
				System.err.println("Unable to find configuration file: " + filename);
				System.exit(0);
			}
			configFile.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
	}

	public String getConfigurationFor(String property){
		return this.configFile.getProperty(property);
	}
	
}
