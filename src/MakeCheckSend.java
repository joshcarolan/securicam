import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

import java.io.File;
import java.util.Vector;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

/**
 * Fa�ade for making the video from the jpg images, checking for a frame with a
 * face in it and then sending the video and the frame via email to the user,
 * sendEmail is done in a new thread(since sending a 10 second video via email
 * attachment takes about 3 minutes with a standard Internet connection in
 * Australia.
 * 
 * @author Joshua Carolan
 * SID:311181058
 * 
 */

public class MakeCheckSend {
	/**
	 * Make - the 10 second movie from the 100 jpg files grabbed. Check - for a
	 * face in the movie Send - send the files to the user
	 * 
	 * @param width
	 *            the width of a frame
	 * @param height
	 *            the height of a frame
	 * @param frameRate
	 *            the framerate of the grabber
	 * @param maxFramesIn10Seconds
	 *            number of frames in 10 seconds
	 * @param movieNum
	 *            how many videos have been recorded in this run of the
	 *            application
	 */

	public void makeCheckAndSend(int width, int height, int frameRate,
			int maxFramesIn10Seconds, int movieNum) {

		// //////////////////////////MAKE//////////////////////////////////////
		Vector inputFiles = new Vector();
		// iterate all files in the project folder
		for (String filename : new File(".").list()) {
			// if it is a jpg, add it to the file list
			if (filename.endsWith("jpg"))
				inputFiles.add(filename);
		}

		String outputURL = "out" + movieNum + ".avi";
		MyJpegImagesToMovie imagesToMovie = new MyJpegImagesToMovie(outputURL,
				width, height, frameRate, inputFiles);
		// //////////////////////////////////////////////////////////////////////

		// /////////////////////////CHECK////////////////////////////////////////
		// check for a face shot to include in the email
		String faceFileName = getFrameWithFace(maxFramesIn10Seconds);
		// //////////////////////////////////////////////////////////////////////

		// /////////////////////////SEND/////////////////////////////////////////
		/*
		 * change the name of the file so it can be sent without possibly being
		 * over written by the camera recording a new video
		 */
		String newFaceFileName = "";
		if (!faceFileName.isEmpty()) {
			newFaceFileName = "face" + movieNum + ".png";

			File faceFile = new File(faceFileName);
			File newFaceFile = new File(newFaceFileName);
			faceFile.renameTo(newFaceFile);

		}

		SendEmail se = new SendEmail(newFaceFileName, outputURL);
		se.t.start();
		// //////////////////////////////////////////////////////////////////////
	}

	/**
	 * Gets the first frame from the video that has a face in it
	 * 
	 * @param maxFramesIn10Seconds
	 *            number of frames in a 10 second video
	 * @return the filename of the jpg image with the face in it, empty string
	 *         if no face exists in the frames of the video
	 */

	public static String getFrameWithFace(int maxFramesIn10Seconds) {
		FaceDetect fd = new FaceDetect();
		String FrameWithFaceFileName = "";
		IplImage frame;
		for (int i = maxFramesIn10Seconds - 1; i > 0; i--) {
			String stri = "" + i;
			if (i < 10) {
				stri = "00" + i;
			} else if (i < 100) {
				stri = "0" + i;
			}
			FrameWithFaceFileName = stri + ".jpg";
			// System.out.println(FrameWithFaceFileName);
			frame = cvLoadImage(FrameWithFaceFileName);
			if (fd.detectFaces(frame)) {
				// save this frame as a shot of the intruders face
				System.out.println("Face Detected in movement");
				return FrameWithFaceFileName;
			}
		}
		return "";
	}

}
